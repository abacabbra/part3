package part3

import (
	"bufio"
	"os"
)

func countFunc(file string, split bufio.SplitFunc) (int, error) {
	f, err := os.Open(file)
	if err != nil {
		return 0, err

	}
	defer f.Close()

	scanner := bufio.NewScanner(f)
	scanner.Split(split)

	var count = 0
	for scanner.Scan() {
		count++
	}
	if err := scanner.Err(); err != nil {
		return 0, err
	}

	return count, nil
}

func CountLines(file string) (int, error) {
	return countFunc(file, bufio.ScanLines)
}

func CountWords(file string) (int, error) {
	return countFunc(file, bufio.ScanWords)
}
